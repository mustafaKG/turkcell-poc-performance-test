/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 6;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [1.0, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "\/-1"], "isController": false}, {"data": [1.0, 500, 1500, "\/customer-service\/api\/customer\/-9"], "isController": false}, {"data": [1.0, 500, 1500, "\/customer-service\/api\/customer-10"], "isController": false}, {"data": [1.0, 500, 1500, "\/logger-service\/api\/logs-36"], "isController": false}, {"data": [1.0, 500, 1500, "\/logger-service\/api\/logs\/-37"], "isController": false}, {"data": [1.0, 500, 1500, "LogPage"], "isController": true}, {"data": [1.0, 500, 1500, "\/customer-service\/api\/customer-11"], "isController": false}, {"data": [1.0, 500, 1500, "\/customer-service\/api\/customer-12"], "isController": false}, {"data": [1.0, 500, 1500, "Update"], "isController": true}, {"data": [1.0, 500, 1500, "\/customer-service\/api\/customer-8"], "isController": false}, {"data": [1.0, 500, 1500, "CustomerPage"], "isController": true}, {"data": [1.0, 500, 1500, "MainPage"], "isController": true}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 80, 0, 0.0, 78.36250000000001, 49, 146, 118.9, 124.80000000000001, 146.0, 56.657223796033996, 131.97218595077905, 27.271130267351275], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["\/-1", 10, 0, 0.0, 117.5, 104, 146, 144.10000000000002, 146.0, 146.0, 10.548523206751055, 10.867863264767934, 4.831306039029536], "isController": false}, {"data": ["\/customer-service\/api\/customer\/-9", 10, 0, 0.0, 63.3, 57, 70, 69.7, 70.0, 70.0, 11.834319526627219, 9.372688609467456, 5.570451183431953], "isController": false}, {"data": ["\/customer-service\/api\/customer-10", 10, 0, 0.0, 55.199999999999996, 49, 59, 58.8, 59.0, 59.0, 12.048192771084338, 5.000470632530121, 5.91820406626506], "isController": false}, {"data": ["\/logger-service\/api\/logs-36", 10, 0, 0.0, 69.00000000000001, 60, 78, 77.6, 78.0, 78.0, 12.180267965895249, 61.62930115712546, 5.1980245127892815], "isController": false}, {"data": ["\/logger-service\/api\/logs\/-37", 10, 0, 0.0, 71.69999999999999, 62, 86, 85.5, 86.0, 86.0, 12.300123001230013, 106.72759071340714, 5.47739852398524], "isController": false}, {"data": ["LogPage", 10, 0, 0.0, 140.7, 122, 156, 155.3, 156.0, 156.0, 11.325028312570781, 155.5687287655719, 9.87622097961495], "isController": true}, {"data": ["\/customer-service\/api\/customer-11", 10, 0, 0.0, 69.6, 57, 83, 82.4, 83.0, 83.0, 11.918951132300357, 8.45267803933254, 8.148884460667462], "isController": false}, {"data": ["\/customer-service\/api\/customer-12", 10, 0, 0.0, 62.900000000000006, 53, 71, 70.9, 71.0, 71.0, 12.150668286755772, 11.855207700486027, 5.31591737545565], "isController": false}, {"data": ["Update", 10, 0, 0.0, 187.7, 162, 203, 202.6, 203.0, 203.0, 10.449320794148381, 21.942553226227798, 16.84850933908046], "isController": true}, {"data": ["\/customer-service\/api\/customer-8", 10, 0, 0.0, 117.7, 104, 140, 138.5, 140.0, 140.0, 11.013215859030838, 10.744338518722467, 4.818281938325991], "isController": false}, {"data": ["CustomerPage", 10, 0, 0.0, 63.3, 57, 70, 69.7, 70.0, 70.0, 11.848341232227487, 9.383793690758294, 5.57705124407583], "isController": true}, {"data": ["MainPage", 10, 0, 0.0, 235.20000000000002, 217, 264, 262.3, 264.0, 264.0, 9.025270758122744, 18.10342396209386, 8.082200473826713], "isController": true}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 80, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
